Package.describe({
  name: "jauniaux:avatar",
  summary: "Consolidated user avatar template",
  version: "0.1.16",
  git: ""
});

Package.onUse(function(api) {
  api.versionsFrom(['METEOR@0.9.4.1', 'METEOR@1.1', 'METEOR@1.2']);
  api.use(['templating', 'stylus', 'reactive-var'], ['client']);
  api.use(['underscore','coffeescript', 'jparker:gravatar@0.3.1'], ['client', 'server']);
  api.addFiles(
    [
      'template/avatar.html',
      'template/avatar.js',
      'template/avatar.coffee',
      'template/avatar.styl'
    ],
    ['client']
  );
  api.addFiles(
    [
      'utils.js',
      'export.js',
      'helpers.js'
    ],
    ['client', 'server']
  );
  api.export('Avatar');
});

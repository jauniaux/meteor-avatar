Template.avatarP.helpers({
  size: ->
    valid = [
      'large'
      'small'
      'extra-small'
    ]
    return if _.contains(valid, @size) then 'avatar-' + @size else ''

  dimensions: ->
    value = undefined
    if @size == 'extra-large'
      value = 160
    else if @size == 'large'
      value = 80
    else if @size == 'small'
      value = 30
    else if @size == 'extra-small'
      value = 20
    else #medium
      value = 50

    return {
        width: value
        height: value
        }

  shape: ->
    valid = [
      'rounded'
      'circle'
    ]
    return if _.contains(valid, @shape) then 'avatar-' + @shape else ''

  class: ->
    return @class

  imageUrl: ->
    url = null
    player = if _.isString(@player) then Players.findOne( {_id:@player}) else @player
    if player?
      userId = player.user
      if userId?
        user = Meteor.users.findOne(userId);
        if user?
          url = Avatar.getUrl(user)
          if url and url.trim() != '' and Template.instance().firstNode
            img = Template.instance().find('img')
            if img.src != url.trim()
              img.style.removeProperty 'display'

    return url

  initialsCss: ->
    css = ''
    if @bgColor
      css += 'background-color: ' + @bgColor + ';'
    if @txtColor
      css += 'color: ' + @txtColor + ';'
    return css

  initialsText: ->
    #console.log @player
    player = if _.isString(@player) then Players.findOne( {_id:@player}) else Players._transform(@player)

    return @initials or player.initials() 


})